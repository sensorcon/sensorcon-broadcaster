package com.sensorcon.AdvertiseTest;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.*;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.sensorcon.LogDatabase.SensorLog;
import com.sensorcon.LogDatabase.SensorLogDataSource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class DeviceControlActivity extends Activity {

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mLEScanner;
    private ScanSettings settings;
    private List<ScanFilter> filters;

    ListView listView;
    ArrayList<String> timestampList;
    ArrayList<String> sensorValList;
    CustomAdapter adapter;

    SensorLogDataSource db;

    float temp = 0;
    float humidity = 0;
    float pressure = 0;
    float light = 0;
    float CO = 0;
    float battery = 0;

    private ProgressDialog progressBar;
    private ProgressDialog progressBarQuery;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();

    private File csv = null;
    private File root = Environment.getExternalStorageDirectory();
    private FileOutputStream out;

    private ArrayList<String> deviceOrderList;
    private ArrayList<Integer> deviceColorList;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        listView = (ListView) findViewById(R.id.listView);
        timestampList = new ArrayList<String>();
        sensorValList = new ArrayList<String>();
        deviceOrderList = new ArrayList<String>();
        deviceColorList = new ArrayList<Integer>();

        db = new SensorLogDataSource(this);
        try {
            db.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Write dummy log to place where this session started so that current sessions can
        // be exported separately from the entire database. Make sure to remove the last
        // dummy log first
        db.deleteLog("01-01-1970 00:00:00 AM", "00:00:00:00:00:00");
        db.createLog("01-01-1970 00:00:00 AM", "00:00:00:00:00:00", 0, 0, 0, 0, 0, 0);

        adapter = new CustomAdapter(this);
        listView.setAdapter(adapter);

        // Initializes Bluetooth adapter.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(this.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);

            finish();
        } else {

            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();

            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build();
            filters = new ArrayList<ScanFilter>();

            // Start scanning for broadcaster
            scanLeDevice();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        db.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_export_session:
                exportCSV(true);
                return true;
            case R.id.menu_export_all:
                exportCSV(false);
                return true;
            case R.id.menu_delete:
                deleteAllLogs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Starts bluetooth scanning
     */
    private void scanLeDevice() {
        mLEScanner.startScan(filters, settings, mScanCallback);
    }

    /*
     * This function is triggered when a bluetooth low energy device is discovered
     * while scanning
     */
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {

            BluetoothDevice device = result.getDevice();

            // Get the name of the device
            String name = device.getName();
            String mac = device.getAddress();
            int rssi = result.getRssi();

            // Get the advertising data
            byte[] adData = result.getScanRecord().getBytes();
            Log.d("test", mac + " " + bytesToHex(adData));
//            Log.d("test", String.valueOf((int)adData[1]));
//            Log.d("test", String.valueOf(adData.length));

            if(adData.length > 3) {

                // Make sure it is the correct device by checking its company
                // Manufacturer ID: 0xFF (-1)
                // Company ID LSB: 0x94 (-97)
                // Company ID MSB: 0x03
                // Product Family: 0x00
                // Product Type: 0x05
                if (((int)adData[4] == -1) && ((int)adData[5] == -97) && ((int)adData[6] == 0x03) && ((int)adData[7] == 0x00) && ((int)adData[8] == 0x05)) {

                    // Check for new device. If it is new, it will be assigned a different color
                    // Get index of device so correct color is picked
                    int deviceIndex = 0;
                    boolean addDevice = true;
                    for (int i = 0; i < deviceOrderList.size(); i++) {
                        if (deviceOrderList.get(i).equals(mac)) {
                            addDevice = false;
                            deviceIndex = i;
                        }
                    }

                    if (addDevice == true) {
                        deviceOrderList.add(mac);
                        deviceIndex = deviceOrderList.size() - 1;
                    }

                    // This is the starting index of the sensor data in the advertising packet
                    int index = 9;

                    // Extract all of the sensor values from the advertising data
                    temp = (float) (((int) adData[index++] & 0x000000FF) + (((int) adData[index++] << 8) & 0x0000FF00)) / 10;
                    humidity = (float) (((int) adData[index++] & 0x000000FF) + (((int) adData[index++] << 8) & 0x0000FF00)) / 10;
                    pressure = (float) (((int) adData[index++] & 0x000000FF) + (((int) adData[index++] << 8) & 0x0000FF00) + (((int) adData[index++] << 16) & 0x00FF0000)) / 10;
                    light = (float) (((int) adData[index++] & 0x000000FF) + (((int) adData[index++] << 8) & 0x0000FF00) + (((int) adData[index++] << 16) & 0x00FF0000) + (((int) adData[index++] << 24) & 0xFF0000)) / 10;
                    CO = (float) (((int) adData[index++] & 0x000000FF) + (((int) adData[index++] << 8) & 0x0000FF00)) / 10;
                    battery = (float) (((int) adData[index++] & 0x000000FF) + (((int) adData[index++] << 8) & 0x0000FF00)) / 100;

                    // FOR DEMO PURPOSES - If temperature is outside acceptable range or both temp and humidity are 0, ignore
                    if((temp == 0) && (humidity == 0)) {

                    } else {

                        // Get a timestamp to display
                        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
                        Date now = new Date();

                        // Set up the display strings
                        String ts = sdf.format(now);
                        ts += "\n" + mac;
                        ts += "\nRSSI: " + rssi + " dBm";
                        ts += "\nBattery: " + battery + " V";

                        String sensors = "Temperature: " + temp + " degrees \n"
                                + "Humidity: " + humidity + "% \n"
                                + "Pressure: " + pressure + "pA \n"
                                + "Light: " + light + "lux \n"
                                + "CO: " + CO + " PPM";

                        // Add the display strings to the adapter list and update the interface
                        timestampList.add(ts);
                        sensorValList.add(sensors);
                        deviceColorList.add(chooseColor(deviceIndex));

                        // Extended timestamp with date for database
                        SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
                        ts = sdf2.format(now);

                        db.createLog(ts, mac, temp, humidity, pressure, CO, light, battery);

                        adapter.notifyDataSetChanged();

                        // Make sure it shows at the bottom of the screen
                        listView.setSelection(adapter.getCount() - 1);
                    }
                }
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Log.e("Scan Failed", "Error Code: " + errorCode);
        }
    };

    private void exportCSV(boolean session) {

        ArrayList<String> lineList = new ArrayList<String>();

        progressBarQuery = new ProgressDialog(this);

        progressBarQuery.setCancelable(false);
        progressBarQuery.setMessage("Querying Database...");
        progressBarQuery.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBarQuery.setIndeterminate(true);

        progressBarQuery.show();

        ArrayList<SensorLog> logs = (ArrayList<SensorLog>) db.getAllLogs();

        progressBarQuery.dismiss();

        if(session == true) {
            ArrayList<SensorLog> sessionLogs = new ArrayList<SensorLog>();

            int index = 0;
            int count = 0;
            for(int i = 0; i < logs.size(); i++) {
                if(logs.get(i).getTimestamp().equals("01-01-1970 00:00:00 AM")) {
                    index = i;
                }

                if(index > 0) {
                    count++;
                }
            }

            for(int i = 0; i < count; i++) {
                sessionLogs.add(logs.get(index++));
            }

            String line = "";

            for(int i = 0; i < sessionLogs.size(); i++) {

                if (!sessionLogs.get(i).getTimestamp().equals("01-01-1970 00:00:00 AM")) {
                    line = sessionLogs.get(i).getTimestamp() + ","
                            + sessionLogs.get(i).getMACAddress() + ","
                            + sessionLogs.get(i).getTemperature() + ","
                            + sessionLogs.get(i).getHumidity() + ","
                            + sessionLogs.get(i).getPressure() + ","
                            + sessionLogs.get(i).getCO() + ","
                            + sessionLogs.get(i).getLight();

                    lineList.add(line);
                }
            }

            generateCSV(lineList);
        } else {
            String line;

            for(int i = 0; i < logs.size(); i++) {
                if (!logs.get(i).getTimestamp().equals("01-01-1970 00:00:00 AM")) {
                    line = logs.get(i).getTimestamp() + ","
                            + logs.get(i).getMACAddress() + ","
                            + logs.get(i).getTemperature() + ","
                            + logs.get(i).getHumidity() + ","
                            + logs.get(i).getPressure() + ","
                            + logs.get(i).getCO() + ","
                            + logs.get(i).getLight();

                    lineList.add(line);
                }
            }

            generateCSV(lineList);
        }
    }

    private void deleteAllLogs() {
        db.deleteAll();
        db.createLog("01-01-1970 00:00:00 AM", "00:00:00:00:00:00", 0, 0, 0, 0, 0, 0);
    }

    /**
     * Creates csv file
     */
    private void generateCSV(final ArrayList<String> lines) {

        progressBar = new ProgressDialog(this);

        progressBar.setCancelable(false);
        progressBar.setMessage("Generating CSV file...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setIndeterminate(false);
        progressBar.setProgress(0);

        progressBar.show();

        new Thread(new Runnable() {
            public void run() {

            progressBar.setMax(lines.size());

            // Create CSV
            if(root.canWrite()) {

                // Get a timestamp to display
                SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy_hh:mm:ss");
                Date now = new Date();

                // Set up the display strings
                String ts = sdf.format(now);
                String fileName = "SensorconDataLog_" + ts + ".csv";

                File dir = new File (root.getAbsolutePath() + "/SensorconDataLog");
                dir.mkdirs();

                csv = new File(dir, fileName);

                try {
                    out = new FileOutputStream(csv);

                    String line;

                    line = "Time,MAC Address,Temperature,Humidity,Pressure,CO,Light\n";

                    // Header
                    out.write(line.getBytes());

                    for(int i = 0; i < lines.size(); i++) {

                        out.write(lines.get(i).getBytes());
                        out.write("\n".getBytes());

                        progressBarStatus++;

                        if ((progressBarStatus % 50) == 0) {
                            progressBarHandler.post(new Runnable() {
                                public void run() {
                                    progressBar.setProgress(progressBarStatus);
                                }
                            });
                        }
                    }

                    out.close();
                    progressBar.dismiss();

                    send();

                } catch (IOException e) {
                    Log.d("error",e.getMessage());
                }
            }
            }
        }).start();
    }

    /**
     * Sends csv to computer
     */
    private void send() {
        Uri fileUri = Uri.fromFile(csv);

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Sensorcon Log Data");
        sendIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        sendIntent.setType("text/html");
        startActivity(sendIntent);
    }

    /**
     * Returns the correct color for the device to display based on the MAC address
     */
    private int chooseColor(int index) {
        int color = 0;

        switch(index) {
            case 0:
                color = getResources().getColor(R.color.color1);
                break;
            case 1:
                color = getResources().getColor(R.color.color2);
                break;
            case 2:
                color = getResources().getColor(R.color.color3);
                break;
            case 3:
                color = getResources().getColor(R.color.color4);
                break;
            case 4:
                color = getResources().getColor(R.color.color5);
                break;
            case 5:
                color = getResources().getColor(R.color.color6);
                break;
            case 6:
                color = getResources().getColor(R.color.color7);
                break;
            default:
                color = getResources().getColor(R.color.color1);
                break;
        }

        return color;
    }

    /*
     * Custom adapter for showing sensor logs
     */
    public class CustomAdapter extends BaseAdapter {
        Context context;
        private LayoutInflater inflater;

        public CustomAdapter(DeviceControlActivity activity) {
            context = activity;
            inflater = ( LayoutInflater )context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return timestampList.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        public class Holder
        {
            TextView timestamp;
            TextView sensors;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            Holder holder = new Holder();
            View rowView;
            rowView = inflater.inflate(R.layout.log_list, null);
            holder.timestamp = (TextView) rowView.findViewById(R.id.tvTimestamp);
            holder.sensors = (TextView) rowView.findViewById(R.id.tvSensors);
            holder.timestamp.setText(timestampList.get(position));
            holder.sensors.setText(sensorValList.get(position));
            holder.timestamp.setTextColor(deviceColorList.get(position));

            return rowView;
        }
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}